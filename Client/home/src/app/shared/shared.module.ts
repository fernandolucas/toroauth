import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxMaskModule } from 'ngx-mask';

import { FormatCurrencyPipe } from './pipes/format-currency/format-currency.pipe';
import { ModalComponent } from './components/modal/modal.component';


@NgModule({
  declarations: [
    FormatCurrencyPipe,
    ModalComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgxMaskModule.forRoot()
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ModalComponent,

    FormatCurrencyPipe
  ],
  providers: [
    FormatCurrencyPipe
  ],
})
export class SharedModule {

}
