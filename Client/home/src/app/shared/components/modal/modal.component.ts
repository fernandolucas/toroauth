import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'fs-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {

  @Input() title: string;
  @Input() description: string;
  @Input() footer: string;
  @Input() open: boolean;
  @Input() hideBtnClose: '0' | '1'; // [ 0: hide | 1 :show ]

  @Output() closeEvent: EventEmitter<Boolean> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  close() {
    this.open = false;
    this.changeComponentValue(this.open);
  }

  changeComponentValue(value) {
    this.closeEvent.emit(value);
  }

}
