import { NgModule } from '@angular/core';

import { SharedModule } from '../shared/shared.module';
import { FooterComponent } from './footer/footer.component';
import { MenuComponent } from './menu/menu.component';
import { RouterModule } from '@angular/router';


@NgModule({
  declarations: [
    FooterComponent,
    MenuComponent
  ],
  imports: [
    SharedModule,
    RouterModule
  ],
  exports: [
    SharedModule,
    FooterComponent,
    MenuComponent
  ],
  providers: []
})
export class CoreModule { }
