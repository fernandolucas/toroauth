export class ActionModel {
   code: string;
   name: string;
   value: number;
   timestamp: number;
}