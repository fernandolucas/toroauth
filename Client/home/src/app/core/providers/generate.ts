
export class Generate {

   static colors() {
      let hex = '';
      do {
         hex = Math.floor(Math.random() * 16777215).toString(16);
      } while (hex === 'ffffff' || hex === '000000' || hex.length < 6);

      return '#' + hex;
   }
}