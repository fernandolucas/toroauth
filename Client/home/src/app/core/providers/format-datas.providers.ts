export class FormatDatasProvider {

  static convertTimestampForHours(timestamp: number) {
    let date = new Date(timestamp * 1000);
    let hous = date.getHours();
    let min = date.getMinutes();
    let seg = date.getSeconds();
    return `${hous}:${min}:${seg}`
  }

}