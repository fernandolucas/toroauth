import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './components/login/login.component';
import { EsqueceuSenhaComponent } from './components/esqueceu-senha/esqueceu-senha.component';

const routes: Routes = [
  { path: 'esqueceu-senha', component: EsqueceuSenhaComponent },
  { path: 'login', component: LoginComponent }
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
