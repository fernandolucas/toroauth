﻿using CLLogger.Custom;
using Serilog;
using Serilog.Events;
using System;

namespace CLLogger
{
    public static class LoggerConfig
    {
      

        public static ILogger WriteLogger(string FileName = null)
        {
            string custom_template = "{Timestamp:yyy-MMM-dd HH:mm:ss} [{Level}]{Message}{NewLine}{Exception}";

            if (String.IsNullOrEmpty(FileName))
            {
                return new LoggerConfiguration()
                    .MinimumLevel.Debug()
                    .Enrich.With(new LoggerDetailCustom())
                    .WriteTo.Console(LogEventLevel.Debug, outputTemplate: custom_template)
                    .CreateLogger();
            }
            else
            {
                custom_template = "{Timestamp:yyy-MM-dd HH:mm:ss} [{Level}]{Message}{NewLine}{Exception}";
                return new LoggerConfiguration()
                    .Enrich.With(new LoggerDetailCustom())
                    .WriteTo.Console(LogEventLevel.Debug, outputTemplate: custom_template)
                    .WriteTo.File($"{FileName}.txt", outputTemplate: custom_template)
                    .CreateLogger();
            }

        }
    }
}
