﻿using Serilog.Core;
using Serilog.Events;
using System.Reflection;

namespace CLLogger.Custom
{
    public class LoggerDetailCustom : ILogEventEnricher
    {
        // LogEvent DB
        public void Enrich(LogEvent logEvent, ILogEventPropertyFactory propertyFactory)
        {
            var appAssembly = Assembly.GetEntryAssembly();
            var name = appAssembly.GetName().Name;
            var version = appAssembly.GetName().Version;

            logEvent.AddPropertyIfAbsent(propertyFactory.CreateProperty("AppName", name));
            logEvent.AddPropertyIfAbsent(propertyFactory.CreateProperty("AppVersion", version));
        }
    }
}
