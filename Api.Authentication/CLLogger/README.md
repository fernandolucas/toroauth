﻿# CLLogger
Classe de Logger

### version
Library 1.0.0.

## Link do projeto
Lá você pode navegar por toda aplicação (http://fernando-developer.com)


```
    public class UserController
    {
        private ILogger _loggerFile;

        /*parametro no WriteLogger(string param) é opcional, caso queira gerar um arquivo.txt*/
        public UserController()
        {
            _loggerFile = LoggerConfig.WriteLogger("log");
        }


        class Colors
        {
            public string Red { get; set; }
            public string Blue { get; set; }
            public string Black { get; set; }

            /* Formato que deseja imprimir o objeto no Logger*/
            public override string ToString()
            {
                return $"VERMELHO: {Red}, AZUL: {Blue}, PRETO:{Black}";
            }
        }

        private void TestLogger()
        {
            string item1 = "teste item 1";
            string item2 = null;
            try
            {
                var colors = new Colors { Red = "Red", Blue = "Blue", Black = "Black" };

                //Exemplo 01:
                _loggerFile.Information("Sucesso : {0:l}, Cores: {@Colors}", item1, colors);
                
                //Exemplo 02:
                _loggerFile.Warning("Descrição: {0}", item1);
               
                //Exemplo 03:
                if (String.IsNullOrEmpty(item2))
                    _loggerFile.Error("Ocorreu um Erro no sistema no item2 valor = {0} ", item2 ?? "null");

            }
            catch (Exception ex)
            {
                //Exemplo 04:
                _loggerFile.Fatal(ex, "Ocorreu um falha no sistema");
                throw;
            }
        }
    }
```