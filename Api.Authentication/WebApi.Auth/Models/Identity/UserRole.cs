
using Microsoft.AspNetCore.Identity;

namespace WebApi.Auth.Models.Identity
{
   public class UserRole : IdentityUserRole<int>
   {
      public User User { get; set; }
      public Role Role { get; set; }

   }
}