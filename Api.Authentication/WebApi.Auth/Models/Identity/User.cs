
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;
using WebApi.Auth.Models.Identity;

namespace WebApi.Auth.Models.Identity
{
   public class User : IdentityUser<int>
   {
      [Column(TypeName = "nvarchar(150)")]
      public string FullName { get; set; }

      public List<UserRole> UserRoles { get; set; }
   }
}