using System;
using AutoMapper;
using WebApi.Auth.Dtos;
using WebApi.Auth.Models.Identity;

namespace WebApi.Auth.Helpers
{
   public class AutoMapperProfiles : Profile
   {
      public AutoMapperProfiles()
      {
         CreateMap<User, UserDto>().ReverseMap();
         CreateMap<User, UserLoginDto>().ReverseMap();
      }

   }
}