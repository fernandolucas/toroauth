namespace WebApi.Auth.Dtos
{
   public class UserDto : UserLoginDto
   {
      public string FullName { get; set; }
      public string Email { get; set; }
   }
}