﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using CLLogger;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Serilog;
using WebApi.Auth.Dtos;
using WebApi.Auth.Models.Identity;


namespace WebApi.Auth.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UserController : ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly IMapper _mapper;
        private ILogger _loggerFile;

        public UserController(
           IConfiguration config,
           UserManager<User> userManager,
           SignInManager<User> signInManager,
           IMapper mapper)
        {
            _config = config;
            _userManager = userManager;
            _signInManager = signInManager;
            _mapper = mapper;
            _loggerFile = LoggerConfig.WriteLogger("log");
        }

        //TODO: Remover após implementaçção Logger
        class Colors
        {
            public string Red { get; set; }
            public string Blue { get; set; }
            public string Black { get; set; }

            public override string ToString()
            {
                return $"VERMELHO: {Red}, AZUL: {Blue}, PRETO:{Black}";
            }
        }

        //TODO: Remover após implementaçção Logger
        private void _testLogger()
        {
            string item1 = "teste item 1";
            string item2 = null;

            var colors = new Colors { Red = "Red", Blue = "Blue", Black = "Black" };

            _loggerFile.Information("Sucesso : {0:l}, Cores: {@Colors}", item1, colors);

            _loggerFile.Warning("Descrição: {0}", item1);

            if (String.IsNullOrEmpty(item2))
                _loggerFile.Error("Ocorreu um Erro no sistema no item2 valor = {0} ", item2 ?? "null");

            var ex = new ApplicationException("Fake exeption");
            _loggerFile.Fatal(ex, "Ocorreu um falha no sistema");
        }

        [HttpGet("GetUser")]
        public IActionResult GetUser()
        {
            _testLogger();
            return Ok(new UserDto());
        }

        [HttpPost("Register")]
        [AllowAnonymous]
        public async Task<IActionResult> RegisterUser(UserDto userDto)
        {
            try
            {
                var user = _mapper.Map<User>(userDto);
                var result = await _userManager.CreateAsync(user, userDto.Password);

                // verificar retorno bd
                var userToReturn = _mapper.Map<UserDto>(user);

                if (result.Succeeded) return Created("GetUser", userToReturn);

                return BadRequest(result.Errors);
            }
            catch (System.Exception ex)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, $"Banco Dados Falhou {ex.Message}");
            }
        }

        [HttpPost("Login")]
        [AllowAnonymous]
        public async Task<IActionResult> PostLogin(UserLoginDto userLoginDto)
        {
            try
            {
                //verifica se existe user
                var user = await _userManager.FindByNameAsync(userLoginDto.UserName);
                //checa a senha do user, 3º (true) parametro bloqueia user por tentativas de acesso
                var result = await _signInManager.CheckPasswordSignInAsync(user, userLoginDto.Password, false);

                if (result.Succeeded)
                {
                    var appUser = await _userManager.Users
                       .FirstOrDefaultAsync(u => u.NormalizedUserName.ToUpper() == userLoginDto.UserName.ToUpper());

                    var userToReturn = _mapper.Map<UserLoginDto>(appUser);
                    return Ok(new
                    {
                        token = GenerateJWToken(appUser).Result,
                        user = userToReturn
                    });
                }

            }
            catch (System.Exception ex)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, $"Banco Dados Falhou {ex.Message}");
            }
            return Unauthorized();
        }

        private async Task<string> GenerateJWToken(User user)
        {

            // verica as autorizações
            var claims = new List<Claim>
         {
            new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
            new Claim(ClaimTypes.Name, user.UserName)
         };

            // verifica as roles
            var roles = await _userManager.GetRolesAsync(user);

            // seta os papeis no objeto claims
            foreach (var role in roles)
            {
                claims.Add(new Claim(ClaimTypes.Role, role));

            }

            // chave
            var key = new SymmetricSecurityKey(Encoding.ASCII
               .GetBytes(_config.GetSection("AppSettings:Secret").Value));

            // algoritimo de criptografia
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

            // descrição do token
            var tokeDescriptior = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.Now.AddDays(1),
                SigningCredentials = creds
            };
            var tokenHandler = new JwtSecurityTokenHandler();
            var token = tokenHandler.CreateToken(tokeDescriptior);
            return tokenHandler.WriteToken(token);
        }
    }

}