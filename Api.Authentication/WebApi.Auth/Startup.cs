using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Serilog;
using WebApi.Auth.Data;
using WebApi.Auth.Models.Identity;

namespace WebApi.Auth
{
   public class Startup
   {
      public Startup(IConfiguration configuration)
      {
         Configuration = configuration;
      }

      public IConfiguration Configuration { get; }

      // This method gets called by the runtime. Use this method to add services to the container.
      public void ConfigureServices(IServiceCollection services)
      {
         services.AddDbContext<DataContext>(x => x.UseSqlite(Configuration.GetConnectionString("DefaultConnection")));
         services.AddAutoMapper(typeof(Startup));

         IdentityBuilder builder = services.AddIdentityCore<User>(options =>
         {
            options.Password.RequireDigit = false; //remove solicitao de digitos obrigatorios
            options.Password.RequireNonAlphanumeric = false; //remove solicitao de Alfanumericos obrigatorios
            options.Password.RequireLowercase = false; //remove solicitao de letras minusculas obrigatorias
            options.Password.RequireUppercase = false; //remove solicitao de letras maiusculas obrigatorias
            options.Password.RequiredLength = 4; // quantidade maxima de caracteres
         });

         // passando o buider services para dentro do contexto IdentityBuilder
         builder = new IdentityBuilder(builder.UserType, typeof(Role), builder.Services);
         builder.AddEntityFrameworkStores<DataContext>(); // informar com qual context estou trabalhando
         builder.AddRoleValidator<RoleValidator<Role>>();
         builder.AddRoleManager<RoleManager<Role>>(); // gerenciador dos papeis
         builder.AddSignInManager<SignInManager<User>>(); // injetando

         services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
         .AddJwtBearer(options =>
         {
            options.TokenValidationParameters = new TokenValidationParameters
            {
               // validacao do emissor, propria api
               ValidateIssuerSigningKey = true,
               // retornar chave
               IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Configuration.GetSection("AppSettings:Secret").Value)),
               ValidateIssuer = false,
               ValidateAudience = false
            };
         });

         services.AddControllers(options =>
         {
            //politica de autorizacao para todas as controllers
            var policy = new AuthorizationPolicyBuilder().RequireAuthenticatedUser().Build();
            options.Filters.Add(new AuthorizeFilter(policy));
         });

         services.AddCors();
      }

      // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
      public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
      {
         if (env.IsDevelopment())
         {
            app.UseDeveloperExceptionPage();
         }

         // app.UseHttpsRedirection();
         app.UseRouting();
         app.UseAuthorization();
         app.UseAuthentication();
      
         app.UseEndpoints(endpoints =>
         {
            endpoints.MapControllers();
         });
      }
   }
}